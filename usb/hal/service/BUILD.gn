# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")
import("//drivers/peripheral/usb/usb.gni")

USB_HAL_PATH =
    rebase_path("//vendor/${product_company}/${product_name}/hals/usb")
cmd = "if [ -f $USB_HAL_PATH/product.gni ]; then echo true; else echo false; fi"
HAVE_USB_HAL_PATH =
    exec_script("//build/lite/run_shell_cmd.py", [ cmd ], "value")
if (HAVE_USB_HAL_PATH) {
  import("$USB_HAL_PATH/product.gni")
} else {
  enable_linux_native_mode = false
}

config("usbd_private_config") {
  include_dirs = []
  if (enable_linux_native_mode) {
    if (defined(defines)) {
      defines += [ "USB_EVENT_NOTIFY_LINUX_NATIVE_MODE" ]
    } else {
      defines = [ "USB_EVENT_NOTIFY_LINUX_NATIVE_MODE" ]
    }
  }
}

config("usbd_public_config") {
  include_dirs = [
    "include",
    "//drivers/peripheral/usb/hal/client/include",
    "//drivers/peripheral/usb/ddk/common/include",
    "//drivers/peripheral/usb/ddk/host/include",
    "//drivers/peripheral/usb/interfaces/ddk/common",
    "//drivers/peripheral/usb/interfaces/ddk/host",
    "//drivers/peripheral/usb/interfaces/ddk/device",
    "//drivers/peripheral/usb/gadget/function/include",
    "//drivers/hdf_core/framework/model/usb/include",
    default_config_path,
  ]
}

ohos_shared_library("usbd") {
  sources = [
    "src/usbd.c",
    "src/usbd_dispatcher.c",
    "src/usbd_function.c",
    "src/usbd_port.c",
    "src/usbd_publisher.c",
  ]

  configs = [ ":usbd_private_config" ]

  public_configs = [ ":usbd_public_config" ]

  deps = [ "//drivers/peripheral/usb/ddk:libusb_core" ]

  external_deps = [
    "hdf_core:libhdf_host",
    "hdf_core:libhdf_ipc_adapter",
    "hdf_core:libhdf_utils",
    "hdf_core:libhdi",
    "hiviewdfx_hilog_native:libhilog",
    "init:libbegetutil",
    "utils_base:utils",
  ]

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_usb"
}
